module.exports = {
  reactStrictMode: true,
  async redirects() {
    return [
      {
        source: '/about',
        destination: '/hello',
      },
    ]
  },
}
